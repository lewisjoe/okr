# OKR Listing

A tiny SPA. Written with VueJS. Plain Vanilla CSS. No CSS Frameworks.

## Highlights:  
1. Responsive
2. Clean minimal UX 
3. One way data-flow with props. Usecase didn't warrant using complex state management concepts. So refrained from using VueX. 

## Assumptions:  

**1. OKRs without a valid parent id is considered as junk data**

Why? The response seems to contain invalid `parent_objective_id` (i.e parent object cannot be found for that child objective) and there's no reasonable way to include these in the UI.

**2. All child objective's are assumed to be having same category as their parents** 

Why? I've choosen to display the category of an objective beside it's name (for aiding one-click filtering). Since the children are already grouped under a parent, I've choosen not to display category tags beside child objectives. This will be wrong, in case child objectives have the possiblity of having a different category.


**3. The nesting is not more than one level deep.** 
Why? For now the nesting logic only searches the top-level parent OKRs. If it's multiple level of nesting, we need to revisit that part of the code. Also, the component for rendering an objective will have to be refactored in such case.


## Project setup
```
yarn install
```

### For Running Development Server
```
yarn serve
```

### Production
```
yarn build
```

### Linting
```
yarn lint
```
