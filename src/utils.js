
export function fetchOKRs() {
    return fetch("https://okrcentral.github.io/sample-okrs/db.json")
        .then(r => r.json())
        .then(body => body.data)
}

export function nestChildOKRs(okrs) {
    // transform flat list to nested tree

    // collect all the parent okrs in a map
    let parentOkrs = new Map();

    okrs.reduce((parentOkrs, okr) => {
        if (!okr.parent_objective_id) { // better than === '' check
        okr.children = [];
        parentOkrs.set(okr.id, okr);
        }
        return parentOkrs;
    }, parentOkrs);
    
    // loop through okrs again and start nesting child okrs under corresponding parents. 
    // this way, we end up transforming with minimal performance penalty | complexity: O(2n)
    okrs.forEach(okr => {
        if (okr.parent_objective_id) {
        var parentOkr = parentOkrs.get(okr.parent_objective_id);

        // in the request, there's junk children with no parents, but with parent_id set. 
        // ignoring theese junk data
        if (!parentOkr) { // if parentOkr not found, ignore.
            return; // do nothing
        }

        parentOkr.children = parentOkr.children || [];
        parentOkr.children.push(okr);
        }
    })

    // now that we have the hashmap of parents ready, convert them back to an array - which makes it easy to render
    okrs = []; // we don't need the okrs variable anymore. reset it to write our transformed value
    parentOkrs.forEach((okr) => {
        okrs.push(okr);
    })

    return okrs;
}

export function getCategories(okrs) {
    let categoriesMap = new Map(); // map to remove duplicates
    let categories = []; // return array

    okrs.forEach(okr => {
        categoriesMap.set(okr.category, true)
    })
    
    // fill categories array
    categoriesMap.forEach((val, key) => {
        categories.push(key);
    })

    return categories;
}


